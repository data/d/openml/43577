# OpenML dataset: Overwatch-competitions-data-7-seasons

https://www.openml.org/d/43577

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Overwatch is a team-based multiplayer first-person shooter video game developed and published by Blizzard Entertainment, which released on May 24, 2016 for PlayStation 4, Xbox One, and Windows. Described as a "hero shooter", Overwatch assigns players into two teams of six, with each player selecting from a roster of nearly 30 characters, known as "heroes", each with a unique style of play whose roles are divided into three general categories that fit their role: Offense, Defense, Tank, and Support. 
Players on a team work together to secure and defend control points on a map or escort a payload across the map in a limited amount of time. 
I discovered this dataset on the Overwatch Subreddit here: https://www.reddit.com/r/Overwatch/comments/7o8hmg/my_friend_has_recorded_every_game_hes_played/
and there is sort of same dataset here:
https://www.kaggle.com/mylesoneill/overwatch-game-records/home
Data was messy, so I try to clean it and make better and easier for visualising and analysing.
Columns as:
time,  result, map, teamrole, character1, character2, character3 and psychological_condition were transformed (clustered) into easy to use format.
Thanks to JustWingIt for collecting this amazing data!
And Myles O'Neill for bringing this data to Kaggle!

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43577) of an [OpenML dataset](https://www.openml.org/d/43577). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43577/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43577/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43577/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

